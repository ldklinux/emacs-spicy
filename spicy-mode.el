;; Keywords for syntax highlighting

(add-to-list 'auto-mode-alist '("\\.pac2\\'" . spicy-mode))

(setq spicy-functions '("to_uint" "split1" "mod" "length" "transient" "convert"))

(setq spicy-keywords '("enum" "type" "unit" "var" "module" "export" "self" "switch" "on"))

(setq spicy-types '("bytes" "uint8" "uint16" "uint32" "uint64" "list"  "const"))

(setq spicy-constants '("%done" "%init" "0x0"))

(setq spicy-functions-regexp (regexp-opt spicy-functions 'words))
(setq spicy-keywords-regexp (regexp-opt spicy-keywords 'words))
(setq spicy-types-regexp (regexp-opt spicy-types 'words))
(setq spicy-constants-regexp (regexp-opt spicy-constants 'words))
(setq spicy-functions nil)
(setq spicy-keywords nil)
(setq spicy-types nil)
(setq spicy-constants nil)

(setq spicy-font-lock-defaults
      `(
        (,spicy-constants-regexp . font-lock-constant-face)
        (,spicy-types-regexp . font-lock-type-face)
        (,spicy-functions-regexp . font-lock-function-name-face)
        (,spicy-keywords-regexp . font-lock-keyword-face)
        )
      )

;; Syntax table
(defvar spicy-syntax-table nil "Syntax table for `spicy-mode'.")
(setq spicy-syntax-table
      (let ((synTable (make-syntax-table)))
        ;; bash-style comments
        (modify-syntax-entry ?# "< b" synTable)
        (modify-syntax-entry ?\n "> b" synTable)
        (modify-syntax-entry ?_ "w" synTable)
        synTable))

;; Hooks
(defvar spicy-mode-hook nil)
(add-hook 'spicy-mode-hook '(lambda ()
     (local-set-key (kbd "RET") 'newline-and-indent)))
(add-hook 'spicy-mode-hook '(lambda ()
    (local-set-key (kbd "TAB") 'self-insert-command)))

;; Mode definition
(define-derived-mode spicy-mode fundamental-mode
  "spicy-mode is a mode for editing Spicy (.pac2) definitions"
  :syntax-table spicy-syntax-table

  (setq font-lock-defaults '((spicy-font-lock-defaults)))
  (setq mode-name "Spicy definition")

  (setq comment-start "#")
  (setq indent-tabs-mode 1)
  (setq default-tab-width 4)
  (setq indent-line-function 'indent-relative)
  (setq tab-width 4)
  (run-hooks 'spicy-mode-hook)
)

(provide 'spicy-mode)
